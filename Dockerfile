# syntax=docker/dockerfile:1
# Pull base image
FROM python:3

# Set enviroment varibales
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

# Set work directory
WORKDIR /code

# Insall dependences
COPY requirements.txt /code/
RUN pip install -r requirements.txt
RUN docker-compose exec web django-crispy-forms

# Copy project
COPY . /code/

ENV DJANGO_DB_NAME=default
ENV DJANGO_SU_NAME=admin
ENV DJANGO_SU_EMAIL=admin@google.com
ENV DJANGO_SU_PASSWORD=admin

RUN python -c "import django; django.setup(); \
   from django.contrib.auth.management.commands.createsuperuser import get_user_model; \
   get_user_model()._default_manager.db_manager('$DJANGO_DB_NAME').create_superuser( \
   username='$DJANGO_SU_NAME', \
   email='$DJANGO_SU_EMAIL', \
   password='$DJANGO_SU_PASSWORD')"