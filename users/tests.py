from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse


class SignupPageTest(TestCase):
    """Test signup page"""

    def setUp(self) -> None:
        """Create url before each test start"""
        url = reverse('signup')
        self.response = self.client.get(url)

    def test_signup_template(self):
        """Testing status code and assert template"""
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'signup.html')


class CustomUserTest(TestCase):
    """Test creation of user"""

    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(
            username='NewTestUser',
            email='NewTestUser@email.com',
            password='NewTestUser21',
            age=21,
        )

        self.assertEqual(user.username, 'NewTestUser')
        self.assertEqual(user.email, 'NewTestUser@email.com')
        self.assertEqual(user.age, 21)
