from django.shortcuts import render, redirect
from django.views.generic import CreateView
from .forms import CustomUserCreationForm
from django.urls import reverse_lazy
from .models import CustomUser


class SignUp(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

# def customUserForm(request, cu_id=0):
#     if request.method == 'POST':
#         custom_user = CustomUser.objects.get(pk=cu_id)
#         form = CustomUserCreationForm(instance=custom_user)
#         return render(request, 'signup.html', {'form': form})
