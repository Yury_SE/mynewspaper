from django import forms

from .models import Article, Comment


class CommentForms(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('article', 'comment', 'author')
