from .models import Article
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.urls import reverse_lazy

from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import CommentForms


# class ArticleListView(ListView):
#     model = Article
#     template_name = 'articles.html'

def article_list(request):
    object_list = Article.objects.all()
    return render(request, 'articles.html', {'object_list': object_list})


class ArticleUpdateView(LoginRequiredMixin, UpdateView):
    model = Article
    template_name = 'article_update.html'
    fields = ['title', 'body']
    login_url = 'login'


# class ArticleDetailView(LoginRequiredMixin, DetailView):
#     model = Article
#     template_name = 'article_detail.html'
#     login_url = 'login'

def article_detail(request, pk):
    object_detail = get_object_or_404(Article, id=pk)
    comments = object_detail.comments.all()
    new_comment = None

    if request.method == 'POST':
        comment_form = CommentForms(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.article = object_detail
            new_comment.save()
    else:
        comment_form = CommentForms()
    return render(request, 'article_detail.html', {'object': object_detail,
                                                   'comments': comments,
                                                   'new_comment': new_comment,
                                                   'comment_form': comment_form
                                                   })


class ArticleDeleteView(LoginRequiredMixin, DeleteView):
    model = Article
    template_name = 'article_delete.html'
    success_url = reverse_lazy('articles')
    login_url = 'login'


class ArticleCreateView(LoginRequiredMixin, CreateView):
    model = Article
    template_name = 'article_create.html'
    fields = ['title', 'body']
    login_url = 'login'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
