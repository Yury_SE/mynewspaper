from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.article_list, name='articles'),
    path('<int:pk>/', views.article_detail, name='article_detail'),
    path('new/', views.ArticleCreateView.as_view(), name='article_create'),
    path('<int:pk>/update', views.ArticleUpdateView.as_view(), name='article_update'),
    path('<int:pk>/delete', views.ArticleDeleteView.as_view(), name='article_delete'),
]